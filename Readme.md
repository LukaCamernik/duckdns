### DuckDNS Updater

Simple utility to call DuckDNS and ensure it has latest IP.
You specify domains to call and token and it will update it, you can also set it to cron job.
Additionally you can specifiy where to put the log and what ip to change to.

IP is optional because empty IP will be then detected by DuckDNS and set to current IP.

## Options
-d `Domains to update (REQUIRED)`

-t `Token for DuckDNS (REQUIRED)`

-ip `What IP to set`

-l `Log File (Default: output into the console)`