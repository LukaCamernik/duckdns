package main

import (
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type Configuration struct {
	LogFile string
	Domains []string
	Ip      string
	Token   string
}

var configuration Configuration
var printHelp bool

type arrayFlags []string

func (i *arrayFlags) String() string {
	return ""
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}

func init() {
	var domains arrayFlags
	var logFile string
	var token string
	var ip string
	flag.Var(&domains, "d", "Which domains to update, you can specify "+
		"multiple by calling same flag multiple times (one is required)")
	flag.StringVar(&token, "t", "", "DuckDNS token (required)")
	flag.StringVar(&ip, "ip", "", "What IP to set in DuckDNS (Defaults to empty, which will be current)")
	flag.StringVar(&logFile, "l", "", "Where to write the log file to, defaults to console output if empty")
	flag.BoolVar(&printHelp, "help", false, "Print this help message.")
	flag.Parse()
	configuration.Domains = domains
	configuration.LogFile = logFile
	configuration.Ip = ip
	configuration.Token = token

	if configuration.LogFile == "" {
		log.SetOutput(os.Stdout)
	} else {
		f, err := os.OpenFile(configuration.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalf("error opening file: %v", err)
		}
		log.SetOutput(f)
	}
	log.SetLevel(log.DebugLevel)
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	log.SetFormatter(customFormatter)
}

func checkRequiredParams() bool {
	if printHelp || len(configuration.Domains) == 0 || configuration.Token == "" {
		fmt.Println("-----------------------------")
		flag.PrintDefaults()
		fmt.Println("-----------------------------")
		return false
	}
	return true
}

func main() {
	if !checkRequiredParams() {
		os.Exit(0)
	}
	for _, domain := range configuration.Domains {
		domainAddress := getAddress(domain)
		if updateIP(domainAddress) {
			log.Info(fmt.Sprintf("Successfully updated %s", domain))
		} else {
			log.Warn(fmt.Sprintf("Domain %s was not updated!", domain))
		}
	}
}

func getAddress(domain string) string {
	address := "https://www.duckdns.org/update?domains=:domain:&token=:token:&ip=:ip:"
	domainAddress := strings.Replace(address, ":token:", configuration.Token, 1)
	domainAddress = strings.Replace(domainAddress, ":ip:", configuration.Ip, 1)
	domainAddress = strings.Replace(domainAddress, ":domain:", domain, 1)
	return domainAddress
}

func updateIP(current string) bool {
	resp, err := http.Get(current)
	if err != nil {
		log.Error(err)
		return false
	}
	defer resp.Body.Close()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error(err)
		return false
	}
	response := string(responseBody)
	if response == "OK" {
		return true
	} else {
		return false
	}
}
